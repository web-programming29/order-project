import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateCustomerDto {
  @Length(5, 50)
  @IsNotEmpty()
  name: string;

  @Min(3)
  @IsNotEmpty()
  age: number;

  @Length(10, 10)
  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  gender: string;

  createdDate: Date;

  updatedDate: Date;

  deletedDate: Date;
}
