import { IsNotEmpty, IsPositive, Length } from 'class-validator';
import { OrderItem } from 'src/orders/entities/order-item';

export class CreateProductDto {
  @Length(4, 50)
  @IsNotEmpty()
  name: string;

  @IsPositive()
  @IsNotEmpty()
  price: number;

  orderItems: OrderItem;

  createdDate: Date;

  updatedDate: Date;

  deletedDate: Date;
}
