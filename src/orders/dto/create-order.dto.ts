import { IsNotEmpty, IsPositive } from 'class-validator';

class CreateOrderItemDto {
  @IsNotEmpty()
  productId: number;
  @IsPositive()
  @IsNotEmpty()
  amount: number;
}

export class CreateOrderDto {
  @IsNotEmpty()
  customerId: number;
  orderItems: CreateOrderItemDto[];
}
